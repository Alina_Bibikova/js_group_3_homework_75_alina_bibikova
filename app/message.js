const express = require('express');
const router = express.Router();
const Caesar = require('caesar-salad').Caesar;

router.post('/encode', (req, res) => {
    const encodeResult = Caesar.Cipher(req.body.password).crypt(req.body.message);
    res.send({encode: encodeResult});
});

router.post('/decode', (req, res) => {
    const decodeResult = Caesar.Decipher(req.body.password).crypt(req.body.message);
    res.send({decode: decodeResult});
});

module.exports = router;